---
title: Des livres qui font du bien au cœur et à l'esprit…
htmlClass: accueil
# Le reste de cette page se trouve dans _includes/layout-accueil.njk
---
{%- extends 'layout-accueil.njk' -%}

{% block baseline %}
Des récits d'aventures alternatives et solidaires
{% endblock %}

{% block introduction %}
Les **Éditions REPAS** proposent des histoires bien concrètes et réelles d'économie sociale et d'autogestion, racontées par celles et ceux qui les vivent. Partez à la découverte de leurs aventures à travers notre collection originale de *Pratiques Utopiques*.

{% endblock %}

{% block chantiers %}
Les éditions REPAS proposent des **stages d'accompagnement à l'écriture collective** pour des structures engagées dans l'économie sociale et solidaire, qui souhaiteraient raconter leur parcours et étoffer notre collection de Pratiques Utopiques.
{% endblock %}



