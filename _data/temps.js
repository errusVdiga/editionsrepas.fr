const { parse } = require('csv-parse')
const { createReadStream } = require('fs')
const { join } = require('path')

const CSV_FILE = join(__dirname, '..', 'le-temps-qui-passe.csv')

const parser = parse({
  delimiter: ',',
  trim: true,
  from_line: 2,
  columns: ['date', 'person', 'duration'],
})

module.exports = function () {
  let total = 0
  let persons = {}

  return new Promise((resolve) => {
    const stream = createReadStream(CSV_FILE).pipe(parser)

    stream.on('error', (error) => console.error(error))

    parser.on('readable', function(){
      let record;
      while ((record = parser.read()) !== null) {
        const [hours, minutes] = record.duration.split(/\D/)
        const duration = (parseFloat(hours || 0) * 60) + parseFloat(minutes || 0)
        const person = String(record.person).toLocaleLowerCase()

        total += duration
        persons[person] = (persons[person] ?? 0) + duration
      }
    })

    stream.on('end', () => {
      const result = {
        total,
        ...persons
      }

      resolve(result)
    })
  })
}
