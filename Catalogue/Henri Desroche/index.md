---
Collection: Presses de l'Economie Sociale
DépotLégal: 2014
Auteur: Jean-François Draperi
Titre: Henri Desroche
SousTitre:  Espérer, Coopérer, (s’)éduquer
Préface: Préface de Davide Lago
ISBN : 978-2-918522-02-7
Pages: 215 pages
Prix: 15
Etat: Disponible
Résumé: |
    Grand éducateur d’adultes, Henri Desroche (1914-1994) est également un sociologue reconnu dans les champs des religions et de la coopération. On lui doit en outre le terme «entreprises de l’économie sociale» pour désigner le nouvel ensemble formé par les coopératives, les mutuelles et les associations en quête d’unité au début des années 1970. Alliant l’action à la réflexion, Henri Desroche fut aussi le fondateur du Collège coopératif (Paris), du Réseau des hautes études des pratiques sociales (RHEPS) et de l’Université coopérative internationale (UCI). Originale et riche, son œuvre est de celles susceptibles de changer le cours d’une vie. Variée et complexe, elle est difficilement accessible.

    L’objet de ce livre est de présenter l’œuvre d’Henri Desroche, d’en montrer la diversité et d’en saisir l’unité. L’ouvrage s’adresse aux étudiants et enseignants de l’économie sociale et solidaire, aux militants coopératifs et mutualistes, aux élus des collectivités territoriales, aux responsables associatifs, aux travailleurs sociaux, éducateurs, animateurs socioculturels.
    Plus largement, il intéresse toute personne qui souhaite passer du renoncement ou de la contestation à l’action, en donnant la force, les raisons et le plaisir d’espérer, de coopérer et de s’éduquer.
Tags:
- Education populaire
- Analyse de pratique
- Economie sociale et solidaire
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/henri-desroche-jean-francois-draperi-1?_ga=2.214571927.1091905683.1671446978-84470218.1590573003
Couverture:
Couleurs:
 PageTitres: '#796ba9'
---
